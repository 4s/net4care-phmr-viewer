package org.net4care.phmrviewer.model;

import java.net.URI;

public class DocumentDossierSearchResult {

	private String updated;
	private URI self;
	private Dossier[] entries;

	public DocumentDossierSearchResult(){	}
	
	public String getUpdated() {
		return updated;
	}
	public void setUpdated(String updated) {
		this.updated = updated;
	}
	
	public URI getSelf() {
		return self;
	}
	public void setSelf(URI self) {
		this.self = self;
	}
	
	public Dossier[] getEntries() { return entries; }
	public void setEntries(Dossier[] entries) { this.entries = entries; }
}
