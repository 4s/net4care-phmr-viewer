package org.net4care.phmrviewer.model;

import java.net.URI;

public class Dossier {

	private String id;
	private URI self;
	private URI related;
	private String updated;
	private String documentName;
	
	public Dossier(){
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public URI getSelf() {
		return self;
	}
	
	public void setSelf(URI self) {
		this.self = self;
	}
	
	public URI getRelated() {
		return related;
	}
	
	public void setRelated(URI related) {
		this.related = related;
	}
	
	public String getUpdated() {
		return updated;
	}
	
	public void setUpdated(String updated) {
		this.updated = updated;
	}

	public void setDocumentName(String name) {
		this.documentName = name;
	}
	
	public String getDocumentName(){
		return this.documentName;
	}
}
