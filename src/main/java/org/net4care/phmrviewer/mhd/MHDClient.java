package org.net4care.phmrviewer.mhd;

import java.net.URI;
import java.net.URLEncoder;

import org.net4care.phmrviewer.model.DocumentDossierSearchResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class MHDClient {

	//@Autowired
	// need to configure a bean --- do that later ;)
	private RestTemplate restTemplate = new RestTemplate();

	@Value("${mhd.url}")
	private String mhdUrl;

	public String getPHMR(String docId) throws Exception {
		return restTemplate.getForEntity(mhdUrl + "/Document/" + URLEncoder.encode(docId, "utf-8") +
				"/?PatientID=1234567890", String.class).getBody();
	}
	
	public DocumentDossierSearchResult search(String patientId, String from, String to) throws Exception {
		URI url = new URI(mhdUrl + "/DocumentDossier/search?PatientID=" + patientId + "&creationTimeFrom=" + from + "&creationTimeTo=" + to);
		return restTemplate.getForEntity(url,  DocumentDossierSearchResult.class).getBody();
	}

	public String getXdsUrls() {
		try {
			return restTemplate.getForEntity(mhdUrl + "/Status/XdsUrls", String.class).getBody();
		}
		catch (Exception ex) {
			return "Cannot connect to MHD Server at: " + mhdUrl;
		}
	}
}
