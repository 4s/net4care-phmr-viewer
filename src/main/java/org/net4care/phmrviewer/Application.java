package org.net4care.phmrviewer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@ComponentScan(basePackages={"org.net4care"})
public class Application {
	public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }
}
