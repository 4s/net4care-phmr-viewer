package org.net4care.phmrviewer.controller;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.net4care.phmrviewer.mhd.MHDClient;
import org.net4care.phmrviewer.model.DocumentDossierSearchResult;
import org.net4care.phmrviewer.model.SearchParams;
import org.net4care.phmrviewer.transformer.PHMR2Html;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ViewController {

	@Autowired
	private PHMR2Html phmr2Html;

	@Autowired
	private MHDClient mhdClient;

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

    @RequestMapping("/show")
    public String show(Model model, @RequestParam("docId") String docId) throws Exception {
    	String phmrAsHtml = "No matching PHMR!";

    	if (docId != null && docId.length() > 0) {
    		String phmr = mhdClient.getPHMR(docId);
    		phmrAsHtml = phmr2Html.transformPHMR(phmr);
    	}

    	model.addAttribute("phmr", phmrAsHtml);

        return "show";
    }

    @RequestMapping(value="/", method=RequestMethod.GET)
    public String index(Model model) throws Exception {
				model.addAttribute("xdsUrls", mhdClient.getXdsUrls());

        return "index";
    }

    @RequestMapping(value="/", method=RequestMethod.POST)
    public String search(Model model, SearchParams params) throws Exception {
    	String from = validateAndEncodeDate(params.getFrom(), false);
    	String to = validateAndEncodeDate(params.getTo(), true);
    	String cpr = validateAndEncodeCpr(params.getCpr());

    	DocumentDossierSearchResult response = mhdClient.search(cpr, from, to);
    	model.addAttribute("entries", response.getEntries());
    	model.addAttribute("cpr", params.getCpr());
    	model.addAttribute("to", params.getTo());
    	model.addAttribute("from", params.getFrom());
			model.addAttribute("xdsUrls", mhdClient.getXdsUrls());

    	return "index";
    }

    private String validateAndEncodeDate(String date, boolean endOfDay) throws Exception {
    	SimpleDateFormat fromFormatter = new SimpleDateFormat("dd.mm.yyyy");
    	SimpleDateFormat toFormatter = new SimpleDateFormat("yyyymmdd");

    	Date d = fromFormatter.parse(date);
    	String res = toFormatter.format(d);

    	return res + ((endOfDay) ? "2359" : "0000");
    }

    private String validateAndEncodeCpr(String cpr) throws Exception {
    	if (cpr == null || cpr.length() != 10) {
    		throw new Exception("CPR must be encoded as 10 digits");
    	}

    	long cprAsInteger = Long.parseLong(cpr);
    	if (cprAsInteger < 0) {
    		throw new Exception("Invalid character in CPR");
    	}

			return URLEncoder.encode(cpr + "^^^&1.2.208.176.1.2&ISO", "utf-8"); // current MedCom encoding
			// return URLEncoder.encode(cpr + "^^^&2.16.840.1.113883.3.4208.100.2&ISO", "utf-8"); // old MedCom encoding
			// return URLEncoder.encode(cpr + "^^^&2.16.840.1.113883.3.1558.2.1&ISO", "utf-8"); // old Net4Care encoding
    }
}
