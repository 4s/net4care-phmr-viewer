package org.net4care.phmrviewer.transformer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class PHMR2Html {
	@Value("classpath:cda.xsl")
	private Resource xsl;
	
	public String transformPHMR(String phmr) {

    try {
      TransformerFactory f = TransformerFactory.newInstance();
      Transformer t = f.newTransformer(new StreamSource(xsl.getInputStream()));
      Source s = new StreamSource(new ByteArrayInputStream(phmr.getBytes(StandardCharsets.UTF_8)));

      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      Result r = new StreamResult(bos);

      t.transform(s, r);

      return new String(bos.toByteArray());
    }
    catch (Exception ex) {
      return "Error transforming PHMR: " + ex.getMessage();
    }
	}
}
