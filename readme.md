# Net4Care PHMR Viewer

The PHMR Viewer is a standalone component for searching and viewing PHMR documents. Technically it can view any CDA document,
as it utilizes the HL7 *cda.xsl* stylesheet to render the document.

For more information see the [wiki page]([http://wiki.4s-online.dk/doku.php?id=net4care:phmr-viewer:overview).

## Governance
The project is governed by 4S, with source control on [Bitbucket](https://bitbucket.org/4s/net4care-phmr-viewer)
and issue tracking in [JIRA](https://issuetracker4s.atlassian.net/browse/NCV).

## Develop
 - Clone project from Bitbucket:  
   `git clone https://bitbucket.org/4s/net4care-phmr-viewer.git`

 - Compile and run tests:  
   `mvn install`

 - Run the server:  
   `java -jar target/phmr-viewer.jar`
   The server is available on [http://localhost:8090](http://localhost:8090).
